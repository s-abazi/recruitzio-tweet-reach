# recruitz.io Tweet Reach Web App


## Getting Started

These instructions will get you a copy of the project up and running on your local machine to review the functionality.


### Prerequisites

The required software is:

- PHP
- Composer
- Laravel PHP modules
- PHP SQLite module


### Installing


Clone the repository and cd into the project

```sh
git clone git@bitbucket.org:s-abazi/recruitzio-tweet-reach.git
cd recruitzio-tweet-reach
```

Install dependencies

```sh
composer install
```

Copy the provided `.env` file to use my configs or edit it t use your own


## Running

We will use the built in web server via Artisan.

```sh
php artisan serve
```

Now go to http://127.0.0.1:8000

## Usage

Enter URL in the field then submit. Wait for the result.


Enjoy!