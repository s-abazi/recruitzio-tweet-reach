<?php
/**
 * Created by PhpStorm.
 * User: shkumbin
 * Date: 9/14/17
 * Time: 12:02 AM
 */

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Models\Tweet;

class TwitterService
{
    private $connection = null;

    public function __construct()
    {
        $connection = new TwitterOAuth(
            env('TWITTER_CONSUMER_KEY'),
            env('TWITTER_CONSUMER_SECRET'),
            env('TWITTER_ACCESS_TOKEN'),
            env('TWITTER_ACCESS_TOKEN_SECRET'));
        //@TODO Error handling with $connection->get("account/verify_credentials");
        $this->connection = $connection;
    }

    private function parseTweetId($url)
    {
        Log::debug("Parsing URL: " . $url);
        $path = trim(parse_url($url, PHP_URL_PATH), '/');
        return last(explode("/", $path)); //@TODO Better parsing

    }

    private function validateTweet($tweet)
    {
        if (is_object($tweet) && property_exists($tweet, 'errors')) {
            $error = last($tweet->errors);
            Log::error("Error occured!", ['error' => $error]);
            abort(400, $error->message);
        }
    }

    private function updateReach($tweetObj)
    {
        $tweet = $this->connection->get('statuses/show/' . $tweetObj->tweetId );
        $this->validateTweet($tweet);
        $retweets = $this->connection->get('statuses/retweets/' . $tweetObj->tweetId);
        $reach = $tweet->user->followers_count;
        foreach ($retweets as $retweet) {
            $reach += $retweet->user->followers_count;
        }
        $tweetObj->reach = $reach;
        $tweetObj->save();
    }

    public function calculateReach($url)
    {
        $id = $this->parseTweetId($url);
        $tweet = Tweet::firstOrCreate(['tweetId' => $id]);
        $twoHoursAgo = Carbon::now()->subHours(2);
        if ($tweet->wasRecentlyCreated || $twoHoursAgo->gte($tweet->updated_at)) {
            $this->updateReach($tweet);
        }

        return json_encode($tweet);
    }

}
