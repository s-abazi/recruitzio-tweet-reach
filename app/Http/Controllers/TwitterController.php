<?php
/**
 * Created by PhpStorm.
 * User: shkumbin
 * Date: 9/14/17
 * Time: 12:31 AM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Services\TwitterService;

class TwitterController extends Controller
{
    protected $twitterService;

    public function __construct(TwitterService $twitterService)
    {
        $this->twitterService = $twitterService;
    }

    public function tweetReach(Request $request)
    {


        $url = $request->input("urlField");
        if ($url) { //@TODO Better error handling
            return $this->twitterService->calculateReach($url);
        }
        abort(400, "Error: URL missing!");
    }

}