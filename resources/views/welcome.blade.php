<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Google Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
    <!-- CSS Reset -->
    <link rel="stylesheet" href="//cdn.rawgit.com/necolas/normalize.css/master/normalize.css">
    <!-- Milligram CSS minified -->
    <link rel="stylesheet" href="//cdn.rawgit.com/milligram/milligram/master/dist/milligram.min.css">

</head>
<body>

<div class="container">
    <h1>Tweet Reach</h1>

    <div class="row">
        <form id="url-form" action=::>
            <label for="urlField">Tweet URL:</label>
            <input placeholder="https://..." id="urlField" type="text">
            <input class="button-primary" value="Send" type="submit">
        </form>
    </div>
    <div id="output" class="row">
    </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript">
    $("document").ready(function () {
        $("#url-form").submit(function () {
            var data = $("#urlField").val();
            console.log(data);
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "http://localhost:8000/api/tweetReach",
                data: {"urlField": data},
                success: function (data) {
                    $("#output").html(
                        "<h2>Status reach: " + data.reach + "</h2>"
                    );
                    console.log("Form submitted successfully.\nReturned json: " + data);
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status == 400) {
                        msg = 'Error occurred with Twitter API request. [400]';
                    } else {
                        msg = 'Unknown error occurred.';
                    }
                    $("#output").html(
                        "<h2>" + msg + "</h2>"
                    );
                },

            });
            return false;
        });
    });
</script>
</body>

</html>
